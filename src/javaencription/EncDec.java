package javaencription;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.SecureRandom;
import java.util.Base64;

public class EncDec {
    public String enCript(String str){    
        String str1 = Base64.getEncoder().encodeToString(str.getBytes());
        String Str2 = randomString(4);
        String strF = Str2 + str1;
        return reV(strF);
    }    
    //Decript String
    public String deCript(String str){
        byte[] decodedBytes = Base64.getDecoder().decode(reV(str.substring(0, str.length() - 4)));
        String strF = new String(decodedBytes);
        return strF;         
    }    
    //Reverse String 
    public String reV(String str){
        StringBuilder input1 = new StringBuilder(); 
        input1.append(str);
        input1 = input1.reverse();
        return input1.toString();
    }
    //Generate Range String
    public String randomString( int len ){
        String AB = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        SecureRandom rnd = new SecureRandom();    
        StringBuilder sb = new StringBuilder( len );
        for( int i = 0; i < len; i++ ) 
            sb.append( AB.charAt( rnd.nextInt(AB.length()) ) );
        return sb.toString();
    }

    public String readFileAsString(String fileName)throws Exception{ 
        String data = ""; 
        data = new String(Files.readAllBytes(Paths.get(fileName))); 
        return data; 
    } 
    
public String getUrlContents(String theUrl){
    
    StringBuilder content = new StringBuilder();
    
    try{
        URL url = new URL(theUrl);
        URLConnection urlConnection = url.openConnection();
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
        String line;
        
        while((line = bufferedReader.readLine()) != null){
            content.append(line + "\n");
        }
        bufferedReader.close();
        
    }catch(Exception e){
        //e.printStackTrace();
        System.out.println("Error Happens");
    }
    return content.toString();
}

//End Class    
} 
