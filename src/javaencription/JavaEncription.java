package javaencription;
 
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class JavaEncription extends Application{ 
    
    public static void main(String[] args){ 
        launch(args);
    }
    
   public void start(Stage stage) throws IOException, Exception {
       
EncDec enc1 = new EncDec();        
//String content = "{'firstName':'John','lastName':'Smith','age':25,'address':{'streetAddress':'21 2nd Street','city':'New York','state':'NY','postalCode':10021},'phoneNumbers':[{'type':'home','number':'212 555-1234'},{'type':'fax','number':'646 555-4567'}]}";
String content = enc1.getUrlContents("https://raw.githubusercontent.com/bzaman-tk/hello-world/master/version");
File file = new File("filename.txt");
FileWriter fw = new FileWriter(file.getAbsoluteFile());
BufferedWriter bw = new BufferedWriter(fw);
bw.write(enc1.enCript(content));
bw.close(); 
// Be sure to close BufferedWriter

String fstr = enc1.readFileAsString("C:\\Users\\BzamaN\\Documents\\NetBeansProjects\\JavaEncription\\filename.txt");
System.out.println(fstr);       
System.out.println(enc1.deCript(fstr));       
       
      Button eBtn = new Button("Encrypt");
      Button dBtn = new Button("Decrypt"); 
      TextField  txtFld = new TextField();
 
      GridPane gridPane = new GridPane(); 
      gridPane.setMinSize(200, 200);
      gridPane.setPadding(new Insets(10, 10, 10, 10));
      gridPane.setVgap(10); 
      gridPane.setHgap(10);
      gridPane.setAlignment(Pos.CENTER); 
      
      gridPane.add(txtFld, 0, 0);
      gridPane.add(eBtn, 1, 0);
      gridPane.add(dBtn, 0, 1); 
      
      
    eBtn.setOnMouseClicked((new EventHandler<MouseEvent>() { 
        public void handle(MouseEvent event) { 
            EncDec enc = new EncDec(); 
            txtFld.setText(enc.enCript(txtFld.getText()));
        } 
    }));
    
    dBtn.setOnMouseClicked((new EventHandler<MouseEvent>() { 
        public void handle(MouseEvent event) { 
            EncDec enc = new EncDec(); 
            txtFld.setText(enc.deCript(txtFld.getText()));
        } 
    }));
      
      
      Scene scene = new Scene(gridPane); 
      stage.setTitle("Sample Application"); 
      stage.setScene(scene); 
      stage.show(); 
   } 
    
}